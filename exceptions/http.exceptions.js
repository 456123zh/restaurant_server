/**
 * 统一错误处理
 * @param status 状态码
 * @param message 错误信息
 * @param errors 错误信息
 * */
class HttpExceptions extends Error{
    constructor(status,message,errors) {
        super();
        this.status = status;
        this.message = message;
        this.errors = errors;
    }
}

module.exports = HttpExceptions;