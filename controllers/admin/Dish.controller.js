const Dish = require("../../models/dish");
const Result = require("../../utils/result.utils");
const DAO = require("../../dao/dao");
const moment = require("moment");
const { Op } = require("sequelize");

//获取菜品列表
exports.getDishList = async (req, res) => {
  const page = parseInt(req.query.page) || 1;
  const pageSize = parseInt(req.query.pageSize) || 10;
  const options = {
    offset: (page - 1) * pageSize,
    limit: pageSize,
    where: { status: 1 },
  };

  let dbRes = await DAO.findAndCountAll(Dish, options);
  if (!dbRes) return res.json(Result.failed("没有菜品!"));
  const result = dbRes.rows;
  const total = dbRes.count;
  res.json(Result.success({ result, total }));
};

//创建菜品
exports.createDish = async (req, res) => {
  const reqData = req.body;
  console.log(reqData);
  console.log(typeof reqData.image);
  if (typeof reqData.image == "object") {
    reqData.image = JSON.stringify(reqData.image);
  }

  let dbRes = await DAO.findOne(Dish, {
    where: {
      name: reqData.name,
    },
  });
  if (dbRes) return res.json(Result.failed("菜名重复!"));
  let dishData = Object.assign({}, reqData);
  await DAO.create(Dish, dishData);
  res.json(Result.success("添加成功!"));
};

//修改菜品信息
exports.reviseDishInfo = async (req, res) => {
  const reqData = req.body;
  console.log(reqData.id);
  let dbRes = await DAO.findOne(Dish, {
    where: {
      id: reqData.id,
    },
  });
  if (!dbRes) return res.json("没有该菜品请添加!");
  let flavorsData = Object.assign({}, reqData.flavors);
  delete reqData.flavors;
  let dishData = Object.assign({}, reqData);
  let dbRet = await DAO.update(
    Dish,
    { ...dishData },
    { where: { id: dishData.id } }
  );
  if (dbRet !== 1 && dbRen !== 1) return res.json(Result.failed("修改失败!"));
  res.json(Result.success("修改成功!"));
};

//修改菜品 起售 停售
exports.reviseDishStatus = async (req, res) => {
  const { id, status } = req.body;
  const dbRes = await DAO.update(Dish, { status: status }, { where: { id } });
  if (dbRes != 1) return res.json(Result.failed("修改失败!"));
  res.json(Result.success("修改成功!"));
};

//批量删除 菜品
exports.deleteDish = async (req, res) => {
  const { ids } = req.body;
  console.log(ids);
  let dbRes = await DAO.delete(Dish, ids);
  if (dbRes == 0) return res.json(Result.failed("没有该菜品!"));
  res.json(Result.success("删除成功!"));
};

//根据菜品查找菜品
exports.findDish = async (req, res) => {
  const { name } = req.query;
  const dbRes = await DAO.findAll(Dish, {
    where: {
      [Op.or]: {
        name: {
          [Op.like]: `%${name}%`,
        },
        description: {
          [Op.like]: `%${name}%`,
        },
      },
    },
  });
  if (dbRes.length == 0) return res.json(Result.recordNotFound("暂无数据!"));
  res.json(Result.success(dbRes));
};
