class BaseResult {
    constructor(code, message) {
        this.code = code;
        this.message = message;
    }

    // 使用static属性定义常量
    static SUCCESS = new BaseResult(200, "成功");
    static FAILED = new BaseResult(403, "失败");
    static RECORD_NOT_FOUND = new BaseResult(401, '查无对应数据')
    static VALIDATE_FAILED = new BaseResult(400, "参数错误");
    static API_NOT_FOUND = new BaseResult(404, '接口不存在')
    static API_BUSY = new BaseResult(700, '操作过于频繁')
}

/**
 * 统一响应 返回结果
 * @param code 状态码
 * @param message 描述信息
 * @param data 数据
 * */
// 使用module.exports导出类
module.exports = class Result {
    constructor(code, message, data) {
        this.code = code
        this.message = message
        this.data = data
    }

    static success(data) {
        return new Result(BaseResult.SUCCESS.code, BaseResult.SUCCESS.message, data)
    }

    static failed(errData) {
        return new Result(BaseResult.FAILED.code, BaseResult.FAILED.message, errData)
    }

    static validateFailed(params) {
        return new Result(BaseResult.VALIDATE_FAILED.code, BaseResult.VALIDATE_FAILED.message, params)
    }

    static recordNotFound(data) {
        return new Result(BaseResult.RECORD_NOT_FOUND.code, BaseResult.RECORD_NOT_FOUND.message, data)
    }
}
