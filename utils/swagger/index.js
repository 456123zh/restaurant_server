/**
 * 配置swagger
 * */

const options = {
    definition: {
        openapi: '3.0.0',
        info: {
            title: '餐厅 API',
            version: '1.0.0'
        }
    },
    apis: ['./routes/*.js']
}

module.exports = options
