const bcrypt = require('bcrypt')

const SALT = 10;

//生成加密后的密码
const hashPassword = (password) => {
    return new Promise((resolve, reject) => {
        bcrypt.hash(password, SALT, (err, encrypted) => {
            if (err) {
                reject(err)
            }
            resolve(encrypted)
        })
    })
}

/**
 * 校验密码
 * @param oldPwd 老密码
 * @param password 新密码
 * */
const matchPassword = (oldPwd, password) => {
    return new Promise(async (resolve, reject) => {
        let match = await bcrypt.compare(password, oldPwd)
        // console.log(match)
        resolve(match)
    })
}

module.exports = {
    hashPassword,
    matchPassword
};

//测试

// async function test1() {
//     const password = 'abc';
//     const hashPwd = await hashPassword(password);
//     console.log(hashPwd)
//     const matchpwd = await matchPassword(hashPwd,'zzz');
//     console.log(matchpwd)
// }
//
// test1();