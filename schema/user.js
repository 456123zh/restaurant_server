const joi = require('joi');

const username = joi.string().alphanum().min(1).max(15).required();
const password = joi.string().alphanum().pattern(/^[\S]{6,12}$/).required();
const name = joi.string().min(1).max(15).required();
const sex = joi.string().required();
const phone = joi.string().pattern(/^1[3456789]\d{9}$/).required()
const idNumber = joi.string();
const createUser = joi.string().required();
//定义登录表单验证规则
exports.req_login_schema = {
    body:{
        username,password
    }
}

//定义注册表单验证规则

exports.req_createUser_schema = {
    body: {
        idNumber,
        username,
        name,
        sex,
        phone,
        createUser
    }
}