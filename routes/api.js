const express = require('express');
const router = express.Router();
const {getDishList} = require('../controllers/index/dish.controller')
const {makeOrder} = require('../controllers/index/order.controller')

//相关菜品
router.get('/dish/list',getDishList)    //获取所有菜品

//下订单
router.post('/order',makeOrder)




/**
 * @swagger
 * /getDishs:
 *   get:
 *     tags:
 *     - 客户端
 *     summary: 获取所有菜品
 *     responses:
 *      200:
 *        description: 获取数据成功!
 * */


module.exports = router;