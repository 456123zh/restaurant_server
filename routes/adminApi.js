const express = require('express');
const router = express.Router();
const expressJoi = require('@escook/express-joi')
const {req_login_schema, req_createUser_schema} = require('../schema/user')
const {
    login,
    createEmployee,
    getEmployeeList,
    reviseEmployeeStatus,
    reviseEmployeeInfo,
    findEmployee
} = require('../controllers/admin/Employee.controller')
const {findDish,getDishList,createDish,reviseDishInfo,reviseDishStatus,deleteDish} = require('../controllers/admin/Dish.controller')
const {getCategoryList,reviseCategoryInfo,reviseCategoryStatus,createCategory} = require('../controllers/admin/Category.controller')
const {getDishFlavorsList} = require('../controllers/admin/Flavors.controller')
const {dishUploadCallback} = require('../controllers/admin/DishUpload')
const multer = require('multer')
const uuid = require('uuid')


/**
 * 用户api
 * */
router.post('/login', expressJoi(req_login_schema), login);
router.post('/employee/save', expressJoi(req_createUser_schema), createEmployee)
router.get('/employee/list', getEmployeeList)
router.post('/employee/status',reviseEmployeeStatus)
router.put('/employee/employeeInfo',reviseEmployeeInfo)
router.get('/employee/fine',findEmployee)


/**
 * 菜品api
 * */
router.get('/dish/list',getDishList)
router.post('/dish/save',createDish)
router.put('/dish/dishInfo',reviseDishInfo)
router.post('/dish/status',reviseDishStatus)
router.delete('/dish/delete',deleteDish)
router.get('/dish/fine',findDish)

/**
 * 菜品口味api
 */
router.get('/dishFlavor/list',getDishFlavorsList)

/**
 * 分类api
 */
router.post('/category/save',createCategory)
router.post('/category/status',reviseCategoryStatus)
router.put('/category/categoryInfo',reviseCategoryInfo)
router.get('/category/list',getCategoryList)


/**
 * 上传api
 */
const adminStorage = multer.diskStorage({
    destination:function(req,files,cd){
        cd(null,'./public/upload')
    },
    filename:function(req,file,cd){
        let fileFormat = (file.originalname).split('.') //取后缀
        //设置保存时的文件名，uuid+后缀
        cd(null,file.originalname)
    }
})
const dishUpload = multer({ storage: adminStorage })
router.post('/dish/upload',dishUpload.single('file'),dishUploadCallback)


//菜品 swagger

/**
 * @swagger
 * /dish/list:
 *   get:
 *     tags:
 *     - 管理端
 *     summary: 获取菜品列表
 *     parameters:
 *     responses:
 *      200:
 *        description: 获取数据成功!
 * */

/**
 * @swagger
 * /dish/dishInfo:
 *   put:
 *     tags:
 *     - 管理端
 *     summary: 编辑菜品信息
 *     parameters:
 *        - name: categoryId
 *          description: 分类id
 *          in: JSON
 *          required: true
 *          type: number
 *        - name: id
 *          description: 菜品id
 *          in: JSON
 *          required: true
 *          type: number
 *        - name: name
 *          description: 菜品名称
 *          in: JSON
 *          required: true
 *          type: number
 *     responses:
 *      200:
 *        description: 获取数据成功!
 * */

/**
 * @swagger
 * /dish/status:
 *   put:
 *     tags:
 *     - 管理端
 *     summary: 修改菜品起售 停售
 *     parameters:
 *        - name: id
 *          description: 菜品id
 *          in: JSON
 *          required: true
 *          type: number
 *        - name: status
 *          description: 状态 1为起售，0为停售
 *          in: JSON
 *          required: true
 *          type: number   
 *     responses:
 *      200:
 *        description: 获取数据成功!
 * */

//用户 swagger

/**
 * @swagger
 * /login:
 *   post:
 *     tags:
 *     - 管理端
 *     summary: 登录
 *     responses:
 *      200:
 *        description: 获取数据成功!
 * */

/**
 * @swagger
 * /employee/save:
 *   post:
 *     tags:
 *     - 管理端
 *     summary: 创建员工
 *     responses:
 *      200:
 *        description: 获取数据成功!
 * */

/**
 * @swagger
 * /employee/list:
 *   post:
 *     tags:
 *     - 管理端
 *     summary: 获取员工列表
 *     responses:
 *      200:
 *        description: 获取数据成功!
 * */

/**
 * @swagger
 * /employee/status:
 *   post:
 *     tags:
 *     - 管理端
 *     summary: 启用、禁用员工账号
 *     parameters:
 *        - name: id
 *          description: 用户id
 *          in: JSON
 *          required: true
 *          type: string
 *        - name: status
 *          description: 状态
 *          in: JSON
 *          required: true
 *          type: number
 *     responses:
 *      200:
 *        description: 获取数据成功!
 * */

/**
 * @swagger
 * /employee/reviseInfo:
 *   put:
 *     tags:
 *     - 管理端
 *     summary: 编辑员工信息
 *     parameters:
 *        - name: id
 *          description: 用户id
 *          in: JSON
 *          required: true
 *          type: string
 *        - name: status
 *          description: 状态
 *          in: JSON
 *          required: true
 *          type: number
 *     responses:
 *      200:
 *        description: 获取数据成功!
 * */

/**
 * @swagger
 * /employee/reviseInfo:
 *   put:
 *     tags:
 *     - 管理端
 *     summary: 根据姓名查询员工信息
 *     parameters:
 *        - name: id
 *          description: 用户id
 *          in: JSON
 *          required: true
 *          type: string
 *        - name: status
 *          description: 状态
 *          in: JSON
 *          required: true
 *          type: number
 *     responses:
 *      200:
 *        description: 获取数据成功!
 * */


module.exports = router;