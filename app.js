require('dotenv').config({path:'.env'})
require('express-async-errors')
const createError = require('http-errors');
const express = require('express');
const path = require('path');
const cookieParser = require('cookie-parser');
const logger = require('morgan');
const i18n = require('i18n');
const initServer = require('./init/initServer');
const initDB = require('./init/initDB');
const cors = require('cors');

//404 和 错误处理
const noMatchMiddleware = require('./middleware/404.middleware')
const errorMiddleware = require('./middleware/error.middleware')

//权限校验
const {authMiddleware} = require('./middleware/admin/auth.middleware')

const mount = require('mount-routes')

const app = express();

//语言配置
i18n.configure({
  locals:['en','zh'],
  directory:__dirname+'/locales',
  queryParameter:'lang',  //设置查询参数
  defaultLocale:'zh',  //设置默认语言
})
app.use(i18n.init)


//配置env
const dotenv = require('dotenv')
dotenv.config();


//使用swagger API文档
const swaggerJsdoc = require('swagger-jsdoc')
const swaggerUi = require('swagger-ui-express')
const options = require('./utils/swagger/index')
const swaggerSpec = swaggerJsdoc(options);
app.use(process.env.DOCS_URL,swaggerUi.serve,swaggerUi.setup(swaggerSpec))

//统一响应机制
const UnifiedResponse = require('./utils/utils.resextra')
app.use(UnifiedResponse)

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

//配置跨域
app.use(cors({
  credentials:true,
  origin:true
}))   //TODO 这里需要经行详细配置
app.use(logger('tiny'));  //http请求日志
app.use(express.json());  //处理请求参数解析
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

//权限接口校验
// app.use(authMiddleware);

//路由加载 会自动把routers目录下的所有路由都挂在上来
//带路径的用法并且可以打印出路由表 true 表示展示路由表在控制台
mount(app,path.join(process.cwd(),'/routes'),true)

//处理无响应 如果没有路径处理就返回 Not Found
app.use(noMatchMiddleware);

// 错误处理
app.use(errorMiddleware)

const main =  async ()=>{
  await initServer(app)   //初始化服务器
  await initDB()   //初始化数据库
}
main().then(()=>{ })

module.exports = app;
