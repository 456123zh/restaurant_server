const dbConnection = require('../models/connection');
const sequelize = require('../models/index')

/**
 * 数据库初始化
 * */


const initDB = () => {
    return new Promise(async (resolve, reject) => {
        try {
            //数据库连接
            await dbConnection();
            //其他
            await sequelize.sync().then(() => {
                console.log('重新建立了表!');
            })
            resolve();
        } catch (error) {
            console.log(error)
            reject();
        }
    })
}

module.exports = initDB;