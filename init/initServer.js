/**
 * 初始化服务器
 * */
const initServer = async (app)=>{
    return new Promise((resolve,reject)=>{
        const PORT = process.env.DEV_PORT || 3333;
        app
            .listen(PORT,()=>{
                console.log(`服务器运行在 http://${process.env.DEV_HOST}:${PORT}`);
                console.log(`接口文档运行在 http://${process.env.DEV_HOST}:${PORT}${process.env.DOCS_URL}`);
            resolve();
        })
            .on('error',(error)=>{
            console.log(error);
            reject();
        })
    })
}

module.exports = initServer;