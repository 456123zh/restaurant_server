const {Op} = require('sequelize');
const sequelize = require('../models/index');
const {data} = require("express-session/session/cookie");
const {options} = require("joi");

//导出DAO 对象
module.exports = {
    findOne: async (model,options = {}) => {
      try {
          const result = await model.findOne(options);
          return result;
      } catch (error) {
          console.error(`Error retrieving data from ${model.name}:`, error)
          throw error;
      }
    },

    findAll: async (model, options = {}) => {
        try {
            const result = await model.findAll(options);
            return result;
        } catch (error) {
            console.error(`Error retrieving data from ${model.name}:`, error)
            throw error;
        }
    },

    findById: async (model, id) => {
        try {
            const result = await model.findByPk(id);
            return result;
        } catch (error) {
            console.error(`Error retrieving data from ${model.name} with id ${id}:`, error)
            throw error;
        }
    },

    create: async (model, data) => {
        try {
            const result = await model.create(data);
            return result;
        } catch (error) {
            console.error(`Error creating data in ${model.name}:`, error);
            throw error;
        }
    },

    update: async (model,data,options={}) => {
        try {
            const result = await model.update(data, options);
            return result;
        } catch (error) {
            console.error(`Error updating data in ${model.name} with data:`, error);
            throw error;
        }
    },

    delete: async (model, ids) => {
        try {
            let whereClause = {};
    
            if (Array.isArray(ids)) {
                whereClause = { id: { [Op.in]: ids } };
            } else {
                whereClause = { id: ids };
            }
    
            const result = await model.destroy({
                where: whereClause
            });
    
            return result;
        } catch (error) {
            console.error(`Error deleting data in ${model.name} with id ${ids}:`, error);
            throw error;
        }
    }, 

    //分页 TODO 可以当作分页
    findAndCountAll: async (model, options = {}) => {
        try {
            const result = await model.findAndCountAll(options);
            return result;
        } catch (error) {
            console.error(`Error retrieving data from ${model.name}:`, error);
            throw error;
        }
    },

}