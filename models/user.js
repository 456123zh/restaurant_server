const { DataTypes } = require('sequelize');
const sequelize = require('./index');

/**用户表 */

const User = sequelize.define('User', {
    id: {
        type: DataTypes.BIGINT,
        primaryKey: true,
        autoIncrement: true,
    },
    openid: {    //微信用于唯一标识
        type: DataTypes.STRING(45),
        allowNull: false,
        unique: true
    },
    name: {  //用户姓名
        type: DataTypes.STRING(32),
        allowNull: false,
    },
    phone: { //手机号
        type: DataTypes.STRING(11),
        allowNull: false,
    },
    sex: { //性别
        type: DataTypes.STRING(2),
        allowNull: false,
        validate: {
            isIn: [['男', '女']] // only allow these values
        }
    },
    id_number: { //身份证号
        type: DataTypes.STRING(18),
    },
    avatar: {    //微信头像路径
        type: DataTypes.STRING(500)
    },
    create_time: {
        type: DataTypes.DATE,
        allowNull: false,
        defaultValue: sequelize.literal('CURRENT_TIMESTAMP'),
    }
}, {
    tableName: 'user',
    timestamps: false,
    underscored:true
})

module.exports = User;