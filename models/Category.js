const { DataTypes} = require('sequelize');
const sequelize = require('./index');
/** 商品分类表 */

const Category = sequelize.define('Category', {
    id: {
        type: DataTypes.BIGINT,
        primaryKey: true,
        autoIncrement: true,
    },
    name: {  //分类名称
        type: DataTypes.STRING(32),
        allowNull: false,
    },
    type: {  //分类类型 1为菜品分类 2为套餐分类
        type: DataTypes.INTEGER,
        validate: {
            isIn: [[1, 2]]
        }
    },
    sort: { //排序字段
        type: DataTypes.INTEGER,
    },
    status: {   //状态
        type: DataTypes.INTEGER,
        allowNull: false,
        validate: {
            isIn: [[0, 1]]
          }
    },
    create_time: {  //创建时间
        type: DataTypes.DATE,
        defaultValue: sequelize.literal('CURRENT_TIMESTAMP'),
    },
    update_time: {  //最后修改时间
        type: DataTypes.DATE,
        defaultValue: sequelize.literal('CURRENT_TIMESTAMP'),
    },
    create_user: {  //创建人id
        type: DataTypes.BIGINT,
    },
    update_user: {  //最后修改人id
        type: DataTypes.BIGINT,
    },
}, {
    tableName: 'category',
    timestamps: false,
    underscored:true
})

module.exports = Category;