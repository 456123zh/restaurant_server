const { DataTypes } = require('sequelize');
const sequelize = require('./index');

/**员工表 */

const Employee = sequelize.define('Employee', {
    id: {
        type: DataTypes.BIGINT,
        primaryKey: true,
        autoIncrement: true,
    },
    name: {
        type: DataTypes.STRING(32),
        allowNull:false
    },
    username: {
        type: DataTypes.STRING(32),
        allowNull: false,
        unique: true,
    },
    password: {
        type: DataTypes.STRING(64),
        allowNull: false,
    },
    phone: {
        type: DataTypes.STRING(11),
    },
    sex: {
        type: DataTypes.STRING(2),
        validate: {
            isIn: [['男', '女']]
          }
    },
    id_number: { //身份证id
        type: DataTypes.STRING(18),
    },
    status: {   //账号状态
        type: DataTypes.INTEGER,
        allowNull: false,
        validate:{
            isIn:[[0,1]] 
        }
    },
    create_time: {  //创建时间
        type: DataTypes.DATE,
        defaultValue: sequelize.literal('CURRENT_TIMESTAMP'),
    },
    update_time: {  //最后修改时间
        type: DataTypes.DATE,
        defaultValue: sequelize.literal('CURRENT_TIMESTAMP'),
    },
    create_user: {  //创建人id
        type: DataTypes.BIGINT,
    },
    update_user: {  //最后修改人id
        type: DataTypes.BIGINT,
    },
}, {    //表名
    tableName: 'employee',
    timestamps: false,
    underscored:true
})

module.exports = Employee;