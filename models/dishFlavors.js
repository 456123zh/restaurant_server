const { DataTypes } = require('sequelize');
const sequelize = require('./index');

const dishFlavors = sequelize.define('dish_flavors', {
    id: {
        type: DataTypes.BIGINT,
        primaryKey: true,
        autoIncrement: true,
    },
    flavors_name: { //口味名称
        type: DataTypes.STRING(32),
        allowNull: false,
    },
})

module.exports = dishFlavors;