const { DataTypes } = require('sequelize');
const sequelize = require('./index');

/**订单表 */
const Order = sequelize.define('Order', {
    id: {
        type: DataTypes.BIGINT,
        primaryKey: true,
        autoIncrement: true
    },
    order_id: {   //订单号
        type: DataTypes.STRING(50),
        allowNull: false
    },
    status: {   //订单状态 1待付款 2待接单 3已接单 4派送中 5已完成 6已取消
        type: DataTypes.INTEGER,
        allowNull: false,
        validate: {
            isIn: [[1, 2, 3, 4, 5, 6]] 
        }
    },
    user_id: {  //用户id
        type: DataTypes.BIGINT,
        allowNull: false,
        references: {
            model: 'user', 
            key: 'id'
        }
    },
    order_time: { //下单时间
        type: DataTypes.DATE,
        allowNull: false
    },
    checkout_time: {    //付款时间
        type: DataTypes.DATE,
    },
    play_method: {   //支付方式 1微信支付 2支付宝支付  
        type: DataTypes.INTEGER,
        allowNull: false,
        validate: {
            isIn: [[1, 2]]
        }
    },
    play_status: {   //支付状态 0未支付 1已支付 2退款 
        type: DataTypes.TINYINT,
        allowNull: false,
        validate: {
            isIn: [[0, 1, 2]]
        }
    },
    total_price: {   //订单金额  
        type: DataTypes.DECIMAL(10, 2),
        allowNull: false
    },
    remark: {   //备注信息
        type: DataTypes.STRING(100),
    },
    phone: {    //手机号
        type: DataTypes.STRING(11),
        allowNull: false
    },
    address: {  //详细地址
        type: DataTypes.STRING(255),
        allowNull: false
    },
    user_name: {    //用户姓名
        type: DataTypes.STRING(32),
        allowNull: false
    },  
    cancel_reason: {    //订单取消原因
        type: DataTypes.STRING(255),
    },
    rejection_reason: { //拒单原因
        type: DataTypes.STRING(255),
    },
    cancel_time: {  //订单取消时间
        type: DataTypes.DATE,
    },
    estimated_delivery_time: {  //预计送达时间
        type: DataTypes.DATE,
    },
    delivery_status: {  //配送状态 1立即送出  0选择具体时间          
        type: DataTypes.TINYINT,
        validate: {
            isIn: [[0, 1]]
        }
    },
    delivery_time: {    //送达时间
        type: DataTypes.DATE,
    },
    pack_amount: {  //打包费
        type: DataTypes.INTEGER,
    },
    tableware_number: { //餐具数量
        type: DataTypes.INTEGER,
    },
    tableware_status: { //餐具状态 1按餐量提供  0选择具体数量
        type: DataTypes.TINYINT,
        validate: {
            isIn: [[0, 1]]
        }
    }
}, {

    timestamps: false,
    underscored: true,
    tableName: 'order',
});

module.exports = Order