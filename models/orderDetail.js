const { DataTypes } = require('sequelize');
const sequelize = require('./index');

/** 订单详情表 */

const OrderDetail = sequelize.define('OrderDetail', {
    id: {
        type: DataTypes.BIGINT,
        primaryKey: true,
        autoIncrement: true
    },
    name: {  //商品名称
        type: DataTypes.STRING(32),
        allowNull: false
    },
    image: {  //商品图片
        type: DataTypes.STRING(255),
    },
    order_id: {  //订单id
        type: DataTypes.BIGINT,
        allowNull: false,
        references: {
            model: 'order', 
            key: 'id'
        }
    },
    dish_id: {  //菜品id
        type: DataTypes.BIGINT,
        references: {
            model: 'dish', 
            key: 'id'
        }
    },
    setmeal_id: {   //套餐id
        type: DataTypes.BIGINT,
        references: {
            model: 'setmeal', 
            key: 'id' 
        }
    },
    dish_flavor: {  //菜品口味
        type: DataTypes.STRING(50),

    },
    number: {   //商品数量
        type: DataTypes.INTEGER,
        allowNull: false
    },
    amount: {   //商品单价
        type: DataTypes.DECIMAL(10, 2),
        allowNull: false
    }
}, {
    timestamps: false,
    underscored: true,
    tableName: 'order_detail'
});


module.exports = OrderDetail;