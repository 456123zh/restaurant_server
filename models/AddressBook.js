const { DataTypes } = require('sequelize');
const sequelize = require('./index');

/**地址表 */
const AddressBook = sequelize.define('AddressBook', {
    id: {
        type: DataTypes.BIGINT,
        primaryKey: true,
        autoIncrement: true
      },
      user_id: {    //用户id
        type: DataTypes.BIGINT,
        allowNull: false,
        references: {
          model: 'user',
          key: 'id'
        }
      },
      consignee: {  //收货人
        type: DataTypes.STRING(50),
        allowNull: false
      },
      sex: {  
        type: DataTypes.STRING(2),
        allowNull: false
      },
      phone: {
        type: DataTypes.STRING(11),
        allowNull: false
      },
      province_code: {  //省份编码
        type: DataTypes.STRING(12),
      },
      province_name: {  //省份名称
        type: DataTypes.STRING(32),
      },
      city_code: {  //城市编码
        type: DataTypes.STRING(12),
      },
      city_name: {  //城市名称
        type: DataTypes.STRING(32),
      },
      district_code: {  //区县编码
        type: DataTypes.STRING(12),
      },
      district_name: {  //区县名称
        type: DataTypes.STRING(32),
        allowNull: false
      },
      detail: { //详细地址信息 具体到门牌号
        type: DataTypes.STRING(200),
        allowNull: false
      },
      label: { //标签 公司、家、学校
        type: DataTypes.STRING(100),
      },
      is_default: { //是否默认地址 1 是 0 否
        type: DataTypes.TINYINT(1),
        allowNull: false,
        defaultValue: 0
      }
}, {
    tableName: 'address_book',
    timestamps: false,
    underscored: true
})

module.exports = AddressBook;
