const {Sequelize} = require("sequelize");

const sequelize = new Sequelize(process.env.DB_NAME, process.env.DB_USERNAME, process.env.DB_PASSWORD, {
    host: process.env.DB_HOST,
    dialect: process.env.DB_DIALECT,
    operatorsAliases: false,
    logging: false,
    timezone: '+08:00', //东八时区
    dialectOptions: {
        //时间格式化 返回字符串
        dateStrings: true,
    },
    pool: {
        max: 5,
        min: 0,
        acquire: process.env.DB_POOL_ACQUIRE,
        idle: process.env.DB_POOL_IDLE
    }
});


module.exports = sequelize;
