const { DataTypes } = require('sequelize');
const sequelize = require('./index');

/**购物车 */
const ShoppingCart = sequelize.define('ShoppingCart', {
    id: {
      type: DataTypes.BIGINT,
      primaryKey: true,
      autoIncrement: true
    },
    name: { //商品名称
      type: DataTypes.STRING(32),
      allowNull: false
    },
    image: {    //商品图片路径
      type: DataTypes.STRING(255),
    },
    user_id: {  //用户id
      type: DataTypes.BIGINT,
      allowNull: false,
      references: {
        model: 'user', 
        key: 'id'
      }
    },
    dish_id: {  //菜品id
      type: DataTypes.BIGINT,
      allowNull: false,
      references: {
        model: 'dish', 
        key: 'id'
      }
    },
    setmeal_id: {   //套餐id
      type: DataTypes.BIGINT,
      allowNull: true,
      references: {
        model: 'setmeal',
        key: 'id' 
      }
    },
    dish_flavor: {  //菜品口味
      type: DataTypes.STRING(50),
    },
    number: { //商品数量
      type: DataTypes.INTEGER,
      allowNull: false
    },
    amount: {   //商品单价
      type: DataTypes.DECIMAL(10,2),
      allowNull: false
    },
    create_time: {
      type: DataTypes.DATE,
      allowNull: false
    }
  }, {
    tableName: 'shopping_cart',
    timestamps: false,
    underscored: true
  });

module.exports = ShoppingCart;