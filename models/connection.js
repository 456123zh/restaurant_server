const sequelize = require('./index')
/**
 * 测试数据库连接
 * */
const dbConnection = async ()=>{
    return new Promise(async (resolve,reject)=>{
        try {
            await sequelize.authenticate();
            console.log('Connection mysql has been established successfully.');
            resolve()
        } catch (error){
            console.error('Unable to connect to the database',error)
            reject(error)
        }
    })
}

module.exports = dbConnection;
