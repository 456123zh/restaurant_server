const { DataTypes } = require('sequelize');
const dishFlavors = require('./dishFlavors');
const sequelize = require('./index');

/** 菜品表 */

const Dish = sequelize.define('Dish', {
    id: {
      type: DataTypes.BIGINT,
      primaryKey: true,
      autoIncrement: true,
    },
    name: { //菜品名称
      type: DataTypes.STRING(32),
      allowNull: false,
      unique: true,
    },
    category_id: {  //分类id
      type: DataTypes.BIGINT,
      references: {
        model: 'category',
        key: 'id'
      }
    },
    price: {    //菜品价格
      type: DataTypes.DECIMAL(10, 2),
      allowNull: false,
    },
    image: {    //图片路径
      type: DataTypes.STRING(255),
    },
    description: {  //菜品描述
      type: DataTypes.STRING(255),
    },
    status: {   //售卖状态
      type: DataTypes.INTEGER,
      allowNull: false,
      validate: {
        isIn: [[0, 1]]
      }
    },
    flavors_id:{ //口味
      type: DataTypes.STRING(255),
      references: {
        model: 'dish_flavors',
        key: 'id'
      }
    },
    create_time: {
      type: DataTypes.DATE,
      defaultValue: sequelize.literal('CURRENT_TIMESTAMP'),
    },
    update_time: {
      type: DataTypes.DATE,
      defaultValue: sequelize.literal('CURRENT_TIMESTAMP'),
    },
    create_user: {
      type: DataTypes.BIGINT,
    },
    update_user: {
      type: DataTypes.BIGINT,
    },
  }, {
    tableName: 'dish',
    timestamps: false,
    underscored:true
  });

  module.exports = Dish;