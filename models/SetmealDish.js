const { DataTypes } = require('sequelize');
const sequelize = require('./index');

/** 套餐菜品关系表 */

const SetmealDish = sequelize.define('SetmealDish', {
    id: {
      type: DataTypes.BIGINT,
      primaryKey: true,
      autoIncrement: true,
    },
    setmeal_id: {   //套餐id
      type: DataTypes.BIGINT,
      allowNull: false,
      references: {
        model: 'setmeal',
        key: 'id'
      }
    },
    dish_id: {  //菜品id
      type: DataTypes.BIGINT,
      allowNull: false,
      references: {
        model: 'dish', 
        key: 'id'
      }
    },
    name: { //菜品名称
      type: DataTypes.STRING(32),
      allowNull: false,
    },
    price: {  
      type: DataTypes.DECIMAL(10, 2),
      allowNull: false,
    },
    copies: {   //菜品份数
      type: DataTypes.INTEGER,
    },
  }, {
    tableName: 'setmeal_dish',
    timestamps: false,
    underscored:true
  });

  module.exports =  SetmealDish;