const { DataTypes } = require('sequelize');
const sequelize = require('./index');

/** 套餐表 */

const Setmeal = sequelize.define('Setmeal', {
    id: {
      type: DataTypes.BIGINT,
      primaryKey: true,
      autoIncrement: true,
    },
    name: {  //套餐名称
      type: DataTypes.STRING(32),
      allowNull: false,
      unique: true,
    },
    category_id: {  //分类id
      type: DataTypes.BIGINT,
      references: {
        model: 'category', 
        key: 'id'
      }
    },
    price: {  
      type: DataTypes.DECIMAL(10, 2),
      allowNull: false,
    },
    image: {
      type: DataTypes.STRING(255),
    },
    description: {
      type: DataTypes.STRING(255),
    },
    status: { //售卖状态
      type: DataTypes.INTEGER,
      allowNull: false,
      validate: {
        isIn: [[0, 1]]
      }
    },
    create_time: {  
      type: DataTypes.DATE,
      allowNull: false,
      defaultValue: sequelize.literal('CURRENT_TIMESTAMP'),
    },
    update_time: {
      type: DataTypes.DATE,
      allowNull: false,
      defaultValue: sequelize.literal('CURRENT_TIMESTAMP'),
    },
    create_user: {
      type: DataTypes.BIGINT,
    },
    update_user: {
      type: DataTypes.BIGINT,
    },
  }, {
    tableName: 'setmeal',
    timestamps: false,
    underscored:true
  });

  module.exports = Setmeal;